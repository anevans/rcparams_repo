import matplotlib
import matplotlib.pyplot as plt
import numpy as np

plt.style.use("defaultlibrc")
x=np.linspace(0,10,1000)
y=x**2+np.random.randn(1000)

fig, ax = plt.subplots(1,1)
ax.plot(x,y)
plt.show()

print(matplotlib.matplotlib_fname())